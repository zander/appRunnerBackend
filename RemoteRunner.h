#ifndef REMOTERUNNER_H
#define REMOTERUNNER_H

#include <QThread>

class Message;

class RemoteRunnerPrivate : public QThread
{
    Q_OBJECT
public:
    explicit RemoteRunnerPrivate(int inputId);

    void closeConnection();

signals:
    void receivedMessage(QByteArray data);

protected:
    void run();

private:
    int m_inputId;
};


/**
 * This is part of the biggest chunk of the server. Running at the user,
 * communicating with the server that is priviledged via the pipes.
 *
 * The SecurityManager and the RemoteRunner are this server process (user owned)
 * and priviledged there is the Runner class which actually starts the applications.
 * A RemoteRunner instance is used to send messages to the priviledged process
 * requesting the setup of the namespaces and actually starting the app in there.
 *
 * Notice that the communication is bi-directional, we have 2 pipes for that.
 * Commands go in one direction and (error) messages come back.
 *
 * As an implementation detail; we use a blocking read (2) to wait for incoming data,
 * which is done in a private thread. This class uses Qt signals to keep
 * everything thread-safe.
 */
class RemoteRunner : public QObject
{
    Q_OBJECT
public:
    RemoteRunner(int inputId, int outputId);
    ~RemoteRunner();

    void runRemote(const Message &message) const;

signals:
    void receivedMessage(QByteArray data);

private:
    RemoteRunnerPrivate m_thread;
    int m_outputId;
};


#endif
