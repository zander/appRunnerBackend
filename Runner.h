#ifndef RUNNER_H
#define RUNNER_H

#include "Message.h"

#include <deque>
#include <filesystem>

void renameThisProcess(char *nameBlob, int blobSize, const char *newName);

class Runner
{
public:
    Runner(const Message &message, int errorFile);

    /**
     * The user Id that owns the security manager.
     */
    void setOwnerUserId(uint32_t uid);
    void setProcessName(char *name, int allocatedSize);

    void addPipe(int fd) {
        m_pipes.push_back(fd);
    }

    void run();

private:
    void sendUpstream(const char *errorMessage);
    bool runCopy(const std::string &from, const std::filesystem::path &to) const;
    bool copySingle(const std::filesystem::path &from, const std::filesystem::path &to) const;
    // copy env, but filter by the details from m_message
    void copyFilteredEnv(char **from, char **target);
    void mkdirs(const std::filesystem::path &dir) const;
    int runInitScript();

    int runEncFs(const char *password, int strlen) const;

    const int m_outputFD;
    uint32_t m_ownerUid = 0;
    uint32_t m_jailId = 0;
    Message m_message;

    char *m_processName = nullptr;
    int m_processNameSize = 0; // the amount of bytes allocated to it.
    std::deque<int> m_pipes;
};

#endif
