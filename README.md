# Isolation-runner

## Motivation

On any (Linux) desktop today, each and every application has full access to all things that the user herself has access to.

If you run only software that is trusted, this is likely OK. And this has been the state we have been in for the last 20 years. Hopefully nothing bad will happen. There are no virus' in Linux, right?

In the last years we have seen the Linux desktop become accepted more by commercial vendors, however. This changes the setup quite dramatically as it shows how dependent our common security is on civility and voluntary code-reviews...

Installing applications using your Linux distro's packaging is quite useful as you depend on the chain-of-custody and they work to keep you free from bad software.

But what about the applications that you get from elsewhere? Linux is getting more popular and there have always been a good amount of applications that only come from '3rd sources'. For instance brave browser. Or some VPN's tools. The list is getting longer every year.

This closed source software breaks the very fragile setup that keeps Linux users safe. Because frankly, the moment a user starts an application on his or her machine, all bets are off. The application can shut down the machine, can break it, can delete all your data etc. Or do more nasty things that you don't actually realize immediately. Upload your browsing history and sell it to the highest bidder, for instance.

## Isolation through Linux namespaces

Linux already has (for nearly a decade) isolation features in the form of namespaces. The basic idea is that using namespaces an application can be completely convinced it is the only application running on the machine. It can be convinced there are no files in your homedir.

This concept has been used on phones for a long time and are just as viable on a desktop. Isolating an application means that your visual-studio code application doesn't even know I have a bitcoin wallet on the computer. So it can't rob me since it doesn't know I have money. Simple. No trust needed.

Using IsolationRunner, Each application can be tied to a profile separating them from everyone else, while keeping your main desktop running without such isolation. So you can still copy paste between apps and all that normal stuff that virtual machines make difficult.

You can even have multiple profiles that use the same application. Imagine two browsing profiles, one for business and one for memes. Yes, Firefox allows you to separate that too. But which do you trust more? One where they are simply not able to lie, or one where you have to trust Firefox not to breach your privacy (by accident)?

## Example setup

In most Linux setups the user by default gets a series of directories like "Downloads", "Pictures" and more.  Additionally every application saves config files in $HOME/.config/

Using the iso (short for isolation) command line util you can start any application and it runs as your normal user, in your standard desktop environment.

It will run in its own little jail with a homedir that is private for this profile. Config files and app-data files are stored here, out of your main desktop setup.

With a basic permissions setup you can also still have access to your homedir, your documents. But not any datafiles from applications. Even if you didn't run them in a jail themselves.

The default is that Firefox starts with an empty homedir, but it reuses your private Downloads directory.

## Conclusion

One of the main driving forces is to have partial, not complete, isolation.
It is about keeping your desktop behaving 99% the same as you are used to it behaving, but the hidden datafiles and config files that applications have now are no longer visible for other applications.

A game you download from the Internet can be started without fear of it even being able to empty out your BitcoinCash wallet. It can't access your clipboard or your email application.

## Usage

First, see [[INSTALL]] on how to install.

### Examples:

`iso -l`

prints the available profiles and highlights which are currently in use.

`iso --exec /usr/bin/firefox --rm`

Starts a new firefox in a jail, and will remove the jail upon the shutdown of the application.


### Known limitations

To make the 'ssh' forwarding work, we assume a path for the ssh-agent that is not standard. Author doesn't think there actually is a standard for ssh-agent settings.

We today use a tool called xdg-dbus-proxy in order to limit the jailed application from resources like your clipboard and being able to control other applications. It has a problem that it doesn't allow the jailed application to create its own dbus. Which means that apps like firefo mostly work, but with limitations. Apps that use KDE simply don't work at all inside those jails.

It is rare that a 3rd party app hits this dbus limitation, though.

# Home

Home is at https://codeberg.org/zander/isolationRunner
