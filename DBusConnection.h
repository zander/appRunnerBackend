#ifndef DBUSCONNECTION_H
#define DBUSCONNECTION_H

#include <QDBusAbstractAdaptor>
#include <QDBusMessage>
#include <QDBusVariant>

class IsolationManager;

class DBusConnection : public QDBusAbstractAdaptor
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.tom.IsolationRunner")
    Q_CLASSINFO("URL", "https://codeberg.org/zander/isolationRunner")
public:
    explicit DBusConnection(IsolationManager *parent);

public slots:
    // simple way to run an application in our setup.
    // this is useful because on dbus introspection will show how to use it.
    void run(const QString &fullPath, const QStringList &arguments, const QDBusMessage &message);
    // a more complex version, which is called by run(), it has more features.
    void run2(const QDBusMessage &message);
    /// returns a list of our profiles.
    QDBusVariant listProfiles(bool verbose) const;
    /// Return an XML with details for a specific profile
    void details(const QString &profile, const QDBusMessage &message) const;
    // Update the permissions for a certain profile.
    // Notice that if a property is not in the list of known properties, we ignore it.
    // it doesn't make sense to reply since the only reply that makes sense is
    // to return a full details() call.
    Q_NOREPLY void setRights(const QString &profile, const QStringList &denied, const QStringList &allowed);

private:
    // generates and returns a failure message.
    void fail(const QDBusMessage &message, const QString &errorMessage) const;
    IsolationManager *m_parent;
};

#endif
