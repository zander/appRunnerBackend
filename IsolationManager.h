#ifndef SECURITYMANAGER_H
#define SECURITYMANAGER_H

#include "RemoteRunner.h"
#include "DBusConnection.h"

#include <QObject>
#include <QSettings>
#include <QDir>
#include <QFileSystemWatcher>


class QWidget;

/**
 * The isolation-manager is the biggest part of the
 * server. It is the listener and it drops root priviledges
 * before even loading Qt.
 *
 * This class uses DBus to listen to any requests and after
 * processing them sends data via the pipes to the second
 * part of the server (largely in main.cpp and Runner).
 *
 */
class IsolationManager : public QObject
{
    Q_OBJECT
public:
    explicit IsolationManager(int inputId, int outputId);

    struct AppEntry {
        int appId = -1;
        QString profileName;
        QString pathToExe;
        QStringList denied;
        QStringList allowed;
        QString initScript;
        QString jailPassword;
        bool autoDelete = false;

        // defaults as read from the rules file
        QMap<QString, bool> defaults;

        bool isAllowed(const QString &tag) const;
        // set the list of denied permissions, filtering out only the known types
        void setDenied(const QStringList &entries);
        // set the list of allowed permissions, filtering out only the known types
        void setAllowed(const QStringList &entries);

        bool isKnownPermission(const QString &perm) const;
    };

    QString startApplicationRequest(AppEntry &dbEntry, const QStringList &arguments);

    enum LookupBehavior {
        OnlyExisting,
        MaybeCreate
    };
    AppEntry lookupApp(const QString &path, LookupBehavior behavior);
    std::unique_ptr<QSettings> startEditApp(const QString &profileName, LookupBehavior behavior);

    struct ProfileInfo {
        int jailId = 0;
        QString name;
        QString exe;
        QDateTime lastRun;
        bool active = false;
    };

    // list profiles and known apps we have hosted in the past.
    QList<ProfileInfo> listProfiles() const;

    QDir dbDir() const;
    QString stateFile(int jailId) const;
    QString pipeFilePath(int jailId) const;
    QString jailDir(int jailId) const;

    QString rulesDir() const;
    void setRulesDir(const QString &dir);

private slots:
    void receivedMessageFromRunner(const QByteArray &data);

private:
    void applyRules(AppEntry &context, Message &message, const QString &ruleFile) const;
    QString expandVars(const AppEntry &context, const QString &path) const;

    RemoteRunner m_runner;
    DBusConnection m_listener;

    QString m_rulesDir;
    QString m_basedir;
    QString m_dbdir;
    int m_nextJailId = 0;
};

class AutoDeleter : public QObject
{
    Q_OBJECT
public:
    explicit AutoDeleter(IsolationManager::AppEntry appEntry, IsolationManager *parent);

private slots:
    void startMonitor();
    void jailClosed(const QString &pipeFile);

private:
    const IsolationManager *m_parent;
    const IsolationManager::AppEntry m_jail;
    int m_try = 0;
    QFileSystemWatcher m_watcher;
};

class DelayedApp : public QObject
{
    Q_OBJECT
public:
    explicit DelayedApp(IsolationManager::AppEntry appEntry, const QStringList &arguments, IsolationManager *parent);

    void askPassword();

private slots:
    void cancelPressed();
    void passwordEntered(const QString &text);

private:
    IsolationManager *m_parent;
    IsolationManager::AppEntry m_jail;
    const QStringList m_arguments;

    QWidget *m_win = nullptr;
};

#endif
