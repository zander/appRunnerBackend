#ifndef MESSAGE_H
#define MESSAGE_H

#include <memory>
#include <string>

struct MountMessage {
    enum Type {
        Remount,
        Umount,
        CreateTmpFs // notice, path goes in 'dst'
    };
    Type type;
    std::string src;
    std::string dst;
};

struct CopyMessage {
    std::string from;
    std::string to;
};

struct DBusMapping {
    std::string from;
    std::string to;
    // there are different mapping rules to be applied.
    bool systemBus = false;
};

/**
 * This class is really a 'run-request', disguised as a message.
 * The message holds all the relevant information for a single application to be started,
 * and can be serialized over the wire (a unix pipe) to the priviledged part of
 * the server so that Runner can start the app in the right context.
 *
 * The message is created by using the builder design-pattern.
 * The setters setPath(), setHomedir(), addArgument(),
 *    addRemount() etc are all simply appending instructions to
 * the message. Order is relevant as the target will execute
 * the instructions in-order.
 *
 * The reader uses the Message::Iterator() for reading purposes.
 */
class Message
{
public:
    static constexpr int MAX_SIZE = 4096;
    Message() = delete;
    Message(int size);
    explicit Message(char *buffer, int bufferSize);
    Message(const Message &other) = default;


    /// Sets path to the executable to be run.
    void setPath(const std::string &path);
    char *path() const;

    /// Append an argument to the path() executable
    void addArgument(const char *string);

    Message &operator=(const Message &other) = default;

    /// debug method (is a no-op unless NDEBUG is not defined)
    void printFields() const;

    /// raw getter of message-data
    char *begin() const;
    /// returns message size
    int size() const;

    void setTry(bool isTry);
    void setJailId(uint32_t jailId);

    void addRemount(const std::string &source, const std::string &destination);
    void addUmountPoint(const std::string &dir);
    void addMountTmpDir(const std::string &dir);
    void addCopy(const std::string &from, const std::string &to);
    void addInitSript(const std::string &text);

    void addEnvToUnset(const std::string &propertyName);
    void addEnvToSet(const std::string &envVar);
    void setJailPassword(const std::string &pwd);

    enum DBusType {
        UserSessionBus,
        SystemBus
    };
    void addDBusProxy(DBusType type, const std::string &from, const std::string &to);

    class Iterator {
    public:
        explicit Iterator(const Message * const message);

        bool isArgument() const;
        bool isNewEnvVar() const;
        bool isEnvVarUnset() const;
        bool isUnmount() const;
        bool isRemount() const;
        bool isCreateTmp() const;
        bool isCopy() const;
        bool isJailId() const;
        bool isInitSript() const;
        bool isJailPwd() const;
        bool isValid() const;
        bool isTry() const {
            return m_isTry;
        }
        bool isDBusMapping() const;

        MountMessage mountData() const;
        const char *argument() const;
        CopyMessage copyData() const;

        DBusMapping dbusMapping() const;

        uint32_t jailId() const;

        char *stringPtr() const;
        int stringLength() const;

        bool next();

    private:
        void checkAvail(int bytes) const;

        const Message *m_parent;
        char *m_cur;
        int m_recordSize;
        bool m_isTry = false;
    };

    Iterator iBegin() const {
        return Iterator(this);
    }

private:
    void addString(char type, const std::string &string);
    void addTag(char type);

    char *m_path = nullptr;

    std::shared_ptr<char> m_buf;
    char *m_writePtr = nullptr;
    int m_reservedSize = 0;
    const char *m_firstArgument = nullptr;
};

#endif
